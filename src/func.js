const getSum = (str1, str2) => {
  const arr = [str1, str2];
  if (
    arr.some((element) => typeof element !== 'string') ||
    arr.some((element) => !Number.isFinite(Number(element)))
  ) {
    return false;
  }

  let res = '';
  let carry = 0;

  const padNumber = Math.max(str1.length, str2.length);

  const firstStr =
    str1.length === padNumber ? str1 : str1.padStart(padNumber, '0');
  const secondStr =
    str2.length === padNumber ? str2 : str2.padStart(padNumber, '0');

  for (let i = padNumber - 1; i >= 0; i--) {
    const firstNum = Number(firstStr[i]);
    const secondNum = Number(secondStr[i]);

    const sum = firstNum + secondNum + carry;
    res = i === padNumber && carry ? carry * 10 + sum + res : sum + res;
  }

  return res;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let resComments = 0;

  const getPosts = (arr) => {
    arr.forEach(({ author, comments }) => {
      if (author === authorName) {
        posts++;
      }

      if (comments) {
        getComments(comments);
      }
    });
  };

  const getComments = (arr) => {
    arr.forEach(({ author }) => {
      if (author === authorName) {
        resComments++;
      }
    });
  };

  getPosts(listOfPosts);

  return `Post:${posts},comments:${resComments}`;
};

const tickets = (people) => {
  let change = 0;
  const ticketCost = 25;

  for (let money of people) {
    if (money - ticketCost > change) {
      return 'NO';
    }

    change += money > ticketCost ? change - (change - ticketCost) : money;
  }

  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
